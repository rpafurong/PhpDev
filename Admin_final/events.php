<!DOCTYPE html>
<html id="events">
	<head>
		<meta charset="utf-8">
		<title>Frontend Development</title>
		<?php include "includes/head.php" ?>
	</head>
	<body>
		<?php include "includes/header.php" ?>
		<div id="colTwo">
			<div id="menu_side">
				<a href="#" id="side_open"><span>;</span>Events</a>
				<div id="sub_menu">
					<span>#</span>
					<input type="text"/>
				</div>
			</div>
		<div>
				<div id="calendar"></div>
			</div>
		</div>
	</body>
</html>
