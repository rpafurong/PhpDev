<link rel="stylesheet" type="text/css" href="final.css">
<link rel="stylesheet" type="text/css" href="js/fullcalendar-2.2.6/fullcalendar.css">

<script src="js/chart.js" type="text/javascript"></script>
<script src="js/jquery-1.11.2.js" type="text/javascript"></script>
<script src="js/fullcalendar-2.2.6/lib/jquery.min.js" type="text/javascript"></script>
<script src="js/fullcalendar-2.2.6/lib/moment.min.js" type="text/javascript"></script>
<script src="js/fullcalendar-2.2.6/fullcalendar.js" type="text/javascript"></script>
<script src="js/script.js" type="text/javascript"></script>