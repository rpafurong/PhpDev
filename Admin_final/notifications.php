<!DOCTYPE html>
<html id="notifications">
	<head>
		<meta charset="utf-8">
		<title>Frontend Development</title>
		<?php include "includes/head.php" ?>
	</head>
	<body>
		<?php include "includes/header.php" ?>
		<div id="colTwo">
			<div id="menu_side">
				<a id="side_open" href="#"><span>s</span>Notifications</a>
				<div id="sub_menu" class="ui_searchlist">
					<span>[</span>
					<span>#</span>
					<input type="text" value="Search"/>
					<ul class="ui_navlist">
						<li>
							<a href="#">
								<img src="img/small_profile.jpg" alt="Persons Name"/>
								<p><strong><em>*</em>John Smith</strong> - 10-11-2014<br/><span>s</span> 2 - <span>:</span> 26 mins ago</p>
							</a>
						</li>
						<li>
							<a href="#">
								<img src="img/small_profile.jpg" alt="Persons Name"/>
								<p><strong>John Smith</strong> - 10-11-2014<br/><span>s</span> 2 - <span>:</span> 26 mins ago</p>
							</a>
						</li>
						<li>
							<a href="#">
								<img src="img/small_profile.jpg" alt="Persons Name"/>
								<p><strong>John Smith</strong> - 10-11-2014<br/><span>s</span> 2 - <span>:</span> 26 mins ago</p>
							</a>
						</li>
						<li>
							<a href="#">
								<img src="img/small_profile.jpg" alt="Persons Name"/>
								<p><strong>John Smith</strong> - 10-11-2014<br/><span>s</span> 2 - <span>:</span> 26 mins ago</p>
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div id="notes">
				<h1>John Smith</h1>
				<img id="user" src="img/small_profile.jpg" alt="Persons Name"/>
				<h2>John Smith<br/><span>Head Creative Director</span></h2>
				<b>Message sent - 26 mins ago</b>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue libero sagittis facilisis aliquam. Nunc rhoncus tortor augue, vitae rutrum libero efficitur eget. Duis ut dolor ut ligula hendrerit condimentum eget a lorem. Maecenas ac mattis dui. Maecenas egestas, mi ac feugiat varius, lorem nisl congue mauris, et iaculis enim sem sit amet ipsum. Cras consequat faucibus ex, ac mollis dui dictum a. Sed porta pellentesque mattis. Morbi ac nibh ut tellus venenatis pretium. Mauris dui mauris, tincidunt et felis eget, convallis molestie turpis. Quisque condimentum gravida mi, ac gravida orci. Integer tincidunt, risus id congue aliquet, quam odio feugiat dolor, quis fermentum tellus enim vel lacus. Suspendisse potenti. Nam tempus finibus massa ac molestie. Donec imperdiet tortor non posuere venenatis. Nulla id egestas est.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue libero sagittis facilisis aliquam. Nunc rhoncus tortor augue, vitae rutrum libero efficitur eget. Duis ut dolor ut ligula hendrerit condimentum eget a lorem. Maecenas ac mattis dui. Maecenas egestas, mi ac feugiat varius, lorem nisl congue mauris, et iaculis enim sem sit amet ipsum. Cras consequat faucibus ex, ac mollis dui dictum a. Sed porta pellentesque mattis. Morbi ac nibh ut tellus venenatis pretium. Mauris dui mauris, tincidunt et felis eget, convallis molestie turpis. Quisque condimentum gravida mi, ac gravida orci. Integer tincidunt, risus id congue aliquet, quam odio feugiat dolor, quis fermentum tellus enim vel lacus. Suspendisse potenti. Nam tempus finibus massa ac molestie. Donec imperdiet tortor non posuere venenatis. Nulla id egestas est.</p>
				<ul id="comments">
					<li>
						<img src="img/small_profile" alt="Persons Name">
						<p><strong>John Smith - 1 hour ago</strong><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue libero sagittis facilisis aliquam. Nunc rhoncus tortor augue, vitae rutrum libero efficitur eget.</p>
					</li>
					<li>
						<img src="img/small_profile" alt="Persons Name">
						<p><strong>John Smith - 1 hour ago</strong><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue libero sagittis facilisis aliquam. Nunc rhoncus tortor augue, vitae rutrum libero efficitur eget.</p>
					</li>
					<li>
						<textarea row="5" placholder="Write a reply..."></textarea>
					</li>
				</ul>
			</div>
		</div>
	</body>
</html>
