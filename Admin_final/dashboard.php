<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Frontend Development</title>
		<?php include "includes/head.php" ?>
	</head>
	<body id="dashboard">
		<?php include "includes/header.php" ?>
		<div id="largeBox">
			<h1>Event Views</h1>
			<canvas id="graph" width="1200px"></canvas>
			<canvas id="graphM" width="800px"></canvas>
			<canvas id="graphS" width="400px"></canvas>
		</div>
		<div id="colWrap">
			<div id="colNotes">
			 	<h1>Notifications</h1>
			 	<a href="#">
			 		<strong>John Smith</strong><br />
			 		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse adipiscing ut metus eget mattis.
					laoreet consequat lacus. 
			 	</a>
			 	<a href="#">
			 		<strong>John Smith</strong><br />
			 		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse adipiscing ut metus eget mattis.
					laoreet consequat lacus. 
			 	</a>
			 	<a href="#">
			 		<strong>John Smith</strong><br />
			 		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse adipiscing ut metus eget mattis.
					laoreet consequat lacus. 
			 	</a>
			 </div>
			<div id="colEvents">
				<h1>Events</h1>
				<ul class="ui_navlist">
					
				</ul>
			</div>
		</div>
	</body>
</html>
