$(function() {
	$('#header_open').on('click', function(){ $('#header').toggleClass('open');	});
	$('#side_open').on('click', function(){	$('#sub_menu').toggleClass('sub_open');	});

	if($('html').attr('id') == "events"){


			var cal = $('#calendar').fullCalendar({
				buttonText:{
					prev: 'Ò',
					next: 'Õ'
				},
				header:{
					left:'prev',
					center:'title',
					right:'next'
				},
				editable:true

			});

			$.fn.getMenu = function(){
				$('#events #sub_menu a').remove();

				var cMonth = cal.fullCalendar('getDate').month()+1;
				var cYear = cal.fullCalendar('getDate').year();
				var cDay = cal.fullCalendar('getDate').day();

				var cMonthFull;
				if(cMonth.toString().length == 2){ cMonthFull = cMonth } else { cMonthFull = '0'+cMonth};

				var found = $.grep(file, function( obj ){
					return obj.menu === ""+cMonth+cYear+"";
					
				});

				found.sort(function( a, b){
					var aDate = new Date(a.start);
					var bDate = new Date(b.start);
					return aDate.getDate() - bDate.getDate();

				});
				$.each( found, function( i, obj ){
					//console.log(i);
					//Yung obj na array, may kanya kanyang { start: "2015-03-15"} kaya pag inoutput mo to
					//console.log(obj.start);
					var objDate = obj.start.split("-"),
						objYear = objDate[0],
						objMonth = objDate[1],
						objDay = objDate[2];
					// 	theDate = new Date(objDate);
					// console.log(theDate);
					//console.log(cMonth + " " + cYear + " " + aDay);
					$('#sub_menu').append('<a href="'+obj.url+'"><strong>'+obj.title+' '+objDay+'-'+objMonth+'-'+objYear+'</strong>'+obj.description+'</a>');
				
				});
			}
 
			$.fn.windowCheck = function(){
				var ww = $(window).width();
				if(ww > 650){
					cal.fullCalendar('changeView', 'month');
				}else if( ww < 650 && ww > 400){
					cal.fullCalendar('changeView', 'basicWeek');
				}else{
					cal.fullCalendar('changeView', 'basicDay');
				}
			}

			$(window).on('resize', function(){
				$().windowCheck();
			});

			var file;

			$.getJSON('json/events.php', function(data){
				file = data;
				cal.fullCalendar('addEventSource', data);
				$().getMenu();
				$().windowCheck();
			});

			$('.fc-prev-button,.fc-next-button').on('click', function(){
				$().getMenu();
			});

			console.log("test");

	}

	$('#addme').on('click', function(){
		$(this).after('<a href="#"><img src="img/event.png" alt="Persons Name" /><p><strong>Rose Walker</strong><br />1 hour ago</p></a>')
	});

	if($('body').attr('id') == "dashboard"){
		$.getJSON('json/graph.php', function(info){

			var data = {
			labels : ["January","February","March","April","May","June","July","August", "September", "October"],
			datasets : [
					{
						fillColor : "rgba(244, 211, 122, .1)",
						strokeColor : "#898989",
						pointColor : "#f4d37a",
						pointStrokeColor : "#fff",
						data : info[0].info
					}
				]
			}

			var ctx = $("#graph").get(0).getContext("2d");

			var options ={ bezierCurve : false, scaleFontStyle : "bold", scaleFontSize : 14 };

			new Chart(ctx).Line(data, options);

			var ctxM = $("#graphM").get(0).getContext("2d");

			new Chart(ctxM).Line(data, options);

			var ctxS = $("#graphS").get(0).getContext("2d");

			new Chart(ctxS).Line(data, options);

		});

		$.getJSON('json/events.php', function(data){
			for(i=0; i<3; i++){
				$('.ui_navlist').append('<li><a href="'+data[i].url+'"><img src="img/small_profile.png" alt="Persons Name" /><p><strong>'+data[i].title+'</strong><br />'+data[i].description+'</p></a></li>')
			}
		});

	}

});