<link rel="stylesheet" type="text/css" href="../header/header.css">
<link rel="stylesheet" type="text/css" href="../signin/signin.css">
<link rel="stylesheet" type="text/css" href="../dashboard/dashboard.css">
<link rel="stylesheet" type="text/css" href="../profile/profile.css">
<link rel="stylesheet" type="text/css" href="../notifications/notifications.css">
<link rel="stylesheet" type="text/css" href="../events/events.css">
<link rel="stylesheet" type="text/css" href="../event_page/event_page.css">
<link rel="stylesheet" type="text/css" href="../ui/form.css">
<link rel="stylesheet" type="text/css" href="../ui/nav_list.css">
<link rel="stylesheet" type="text/css" href="../js/fullcalendar-2.2.6/fullcalendar.css">

<script src="../js/chart.js" type="text/javascript"></script>
<script src="../js/jquery-1.11.2.js" type="text/javascript"></script>
<script src="../js/fullcalendar-2.2.6/lib/jquery.min.js" type="text/javascript"></script>
<script src="../js/fullcalendar-2.2.6/lib/moment.min.js" type="text/javascript"></script>
<script src="../js/fullcalendar-2.2.6/fullcalendar.js" type="text/javascript"></script>
<script src="../js/script.js" type="text/javascript"></script>