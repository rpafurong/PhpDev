<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Frontend Development</title>
		<?php include "../head.php" ?>
	</head>
	<body id="profile">
		<?php include "../header/header.php" ?>
		<div id="profile_header">
			<div><img src="profile.jpg" alt="Persons Name"></div>
			<div id="header_txt">
				<h1>John Smith</h1>
				<strong>2 Years - Creative Director</strong>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.
			</div>
		</div>
	</body>
	<div id="profile_main">
		<div id="left">
			<h2>Aboout...</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.</p>
		</div>
		<div id="right">
			<ul class="ui_navlist">
				<li>
					<a href="#">
						<img src="small_profile.jpg" alt="Persons Name"/>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.</p>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="small_profile.jpg" alt="Persons Name"/>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.</p>
					</a>
				</li>
				<li>
					<a href="#">
						<img src="small_profile.jpg" alt="Persons Name"/>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel quam a libero accumsan molestie.</p>
					</a>
				</li>
			</ul>
		</div>
	</div>
</html>
