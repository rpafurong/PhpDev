<!DOCTYPE html>
<html id="eventp">
	<head>
		<meta charset="utf-8">
		<title>Frontend Development</title>
		<?php include "../head.php" ?>
	</head>
	<body>
		<?php include "../header/header.php" ?>
		<div id="colTwo">
			<div id="menu_side">
				<a href="#" id="side_open"><span>;</span>Attendees</a>
				<div id="sub_menu">
					<a href="#" id="addme"><span>Á</span>Add me</a>
					<a href="#">
						<img src="event.png" alt="Persons Name" />
						<p><strong>Martin Walker</strong><br />1 hour ago</p>
					</a>
				</div>
			</div>
			<div id="epm">
				<div>
					<img src="event.png" alt="Persons Name" />
					<h1>John Smith - London Meetup 2015</h1>
					<small>Event date: 02-12-2015</small>
				</div>
				<img src="event_img.png" alt="Event Name">
				<p>
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse adipiscing ut metus eget mattis.Donec euismod adipiscing rutrum. Nunc nulla mi, dignissim vitae leo sed, facilisis condimentum erat. Donec et fermentum tellus,
					laoreet consequat lacus. Cras congue mauris vel erat dignissim porta. Duis et semper tellus. Nulla a leo dolor. Mauris id condimentum velit, in dapibus arcu. Vestibulum id erat at dui accumsan posuere. Aenan rhoncus vestibulum convallis.
					Vivamus aliquet dignissim tempor. Curabitur pulvinar orci urna, id viverra tortor varius at. Sed et tincidunt nisl.
				</p>
				<ul id="comments">
					<li>
						<img src="event.png" alt="Persons Name">
						<p><strong>John Smith - 1 hour ago</strong><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue libero sagittis facilisis aliquam. Nunc rhoncus tortor augue, vitae rutrum libero efficitur eget.</p>
					</li>
					<li>
						<img src="event.png" alt="Persons Name">
						<p><strong>John Smith - 1 hour ago</strong><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer congue libero sagittis facilisis aliquam. Nunc rhoncus tortor augue, vitae rutrum libero efficitur eget.</p>
					</li>
					<li>
						<textarea row="3" placholder="Write a reply..."></textarea>
					</li>
				</ul>
			</div>
		</div>
	</body>
</html>
